#Parser Quake 3 arena

##Objetivo
Construir um programa que retira informações de um arquivo de log do jogo Quake 3 Arena. O programa retira ingormações como numero de jogos, nomes dos jogadores por jogo, mortes por jogador, e tipos de morte.

##Compilando
O programa pode ser compilado utilizando makefile presente no projeto, batando digitar o comando "make" pelo terminal dentro do diretorio do arquivo "main.cpp". 

##Executando o programa
No diretorio onde o programa foi compilado digite o sequinte comando:

	./parser games.log

##Resultados
Ao executar p programa utilizando o comando descrito sera impresso no terminal e no arquivo relatorio.txt. Nele estão descritos todos os jogos do arquivo games.log. Cada jogo é descrito da sequinte forma:

	-----------------------------------
	START_GAME_21
	Jogadores
	Isgalamido	 kills:	 17	killed	 19
	  Oootsimo	 kills:	 22	killed	 17
	Dono da Bola	 kills:	 14	killed	 17
	Assasinu Credi	 kills:	 19	killed	 27
	       Zeh	 kills:	 19	killed	 15
	       Mal	 kills:	  6	killed	 30
	total de mortes:	131

	tipos de morte
	deaths by MOD_UNKNOWN -   0
	deaths by MOD_SHOTGUN -   4
	deaths by MOD_GAUNTLET -   0
	deaths by MOD_MACHINEGUN -   4
	deaths by MOD_GRENADE -   0
	deaths by MOD_GRENADE_SPLASH -   0
	deaths by MOD_ROCKET -  37
	deaths by MOD_ROCKET_SPLASH -  60
	deaths by MOD_PLASMA -   0
	deaths by MOD_PLASMA_SPLASH -   0
	deaths by MOD_RAILGUN -   9
	deaths by MOD_LIGHTNING -   0
	deaths by    MOD_BFG -   0
	deaths by MOD_BFG_SPLASH,MOD_WATER -   0
	deaths by  MOD_SLIME -   0
	deaths by   MOD_LAVA -   0
	deaths by  MOD_CRUSH -   0
	deaths by MOD_TELEFRAG -   0
	deaths by MOD_FALLING -   0
	deaths by MOD_SUICIDE -   3
	deaths by MOD_TARGET_LASER -   0
	deaths by MOD_TRIGGER_HURT -   0
	deaths by MOD_GRAPPLE -  14
	END_GAME_21
	-----------------------------------
 
