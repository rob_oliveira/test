#include <fstream>
#include <string>
#include <iostream>
#include <cstdlib>
#include <vector>

#ifdef MISSIONPACK
#define DEATH_TYPES 28
#else	
#define DEATH_TYPES 23
#endif


using namespace::std;


ifstream read_log;
ofstream write;


// means of death
typedef enum {
    MOD_UNKNOWN,
    MOD_SHOTGUN,
    MOD_GAUNTLET,
    MOD_MACHINEGUN,
    MOD_GRENADE,
    MOD_GRENADE_SPLASH,
    MOD_ROCKET,
    MOD_ROCKET_SPLASH,
    MOD_PLASMA,
    MOD_PLASMA_SPLASH,
    MOD_RAILGUN,
    MOD_LIGHTNING,
    MOD_BFG,
    MOD_BFG_SPLASH,
    MOD_WATER,
    MOD_SLIME,
    MOD_LAVA,
    MOD_CRUSH,
    MOD_TELEFRAG,
    MOD_FALLING,
    MOD_SUICIDE,
    MOD_TARGET_LASER,
    MOD_TRIGGER_HURT,
#ifdef MISSIONPACK
    MOD_NAIL,
    MOD_CHAINGUN,
    MOD_PROXIMITY_MINE,
    MOD_KAMIKAZE,
    MOD_JUICED,
#endif
    MOD_GRAPPLE
} meansOfDeath_t;

char death_causes[DEATH_TYPES][25] = {
	"MOD_UNKNOWN",
    "MOD_SHOTGUN",
    "MOD_GAUNTLET",
    "MOD_MACHINEGUN",
    "MOD_GRENADE",
    "MOD_GRENADE_SPLASH",
    "MOD_ROCKET",
    "MOD_ROCKET_SPLASH",
    "MOD_PLASMA",
    "MOD_PLASMA_SPLASH",
    "MOD_RAILGUN",
    "MOD_LIGHTNING",
    "MOD_BFG",
    "MOD_BFG_SPLASH,"
    "MOD_WATER",
    "MOD_SLIME",
    "MOD_LAVA",
    "MOD_CRUSH",
    "MOD_TELEFRAG",
    "MOD_FALLING",
    "MOD_SUICIDE",
    "MOD_TARGET_LASER",
    "MOD_TRIGGER_HURT",
#ifdef MISSIONPACK
    "MOD_NAIL",
    "MOD_CHAINGUN",
    "MOD_PROXIMITY_MINE",
    "MOD_KAMIKAZE",
    "MOD_JUICED",
#endif
    "MOD_GRAPPLE"
};

class Player{
public:
	Player(string p_name, int pid){
		player_name = p_name;
		kills = 0;
		killed = 0;
		id = pid;
		for (int i = 0; i < DEATH_TYPES; ++i){
			kill_by[i] = 0;
			death_by[i] = 0;
		}
	}

	int kill(int item){
		kills++;
		kill_by[item]++;
		return kills;
	}

	int death(int item){
		killed++;
		death_by[item]++;
		return killed;
	}

	int suicide(int item){
		kills--;
		killed++;
		death_by[item]++;
		return killed;
	}

	string getPlayerStatus(){
		char buff[100];
		sprintf(buff,"%10s\t kills:\t%3d\tkilled\t%3d\n", player_name.c_str(), kills,killed);
		return string(buff);
	}

	string player_name;
	int id;
	int kills;	
	int killed;
	int kill_by[DEATH_TYPES];
	int death_by[DEATH_TYPES];
};
//***fim da classe player


class Game{
public:
	Game(int game_num){
		number_of_games = game_num;
		total_kills = 0;
		sprintf(game_msg, "-----------------------------------\nSTART_GAME_%d", number_of_games);
		cout << game_msg << "\n";
		write << game_msg << "\n";
		for (int i = 0; i < DEATH_TYPES; ++i){
			death_by[i] = 0;
		}
	}
	~Game(){
		sprintf(game_msg, "END_GAME_%d\n-----------------------------------\n", number_of_games);
		vector<string> stats = getPlayersStatus();
		cout<<"Jogadores\n";
		write<<"Jogadores\n";
		for (int i = 0; i < (int)stats.size(); ++i){
			cout <<stats[i];
			write <<stats[i];
		}
		cout <<"total de mortes:\t"<< total_kills << "\n";
		write<<"total de mortes:\t"<< total_kills <<"\n";
		
		cout <<"\ntipos de mortes\n";
		write<<"\ntipos de morte\n";
		
		string *causes = deathCauses();
		for (int i = 0; i < DEATH_TYPES; ++i){
			cout<<causes[i];
			write<<causes[i];
		}
		total_kills = 0;
		delete[] causes;
		cout <<game_msg<<endl;
		write<<game_msg<<endl;
	}

	vector<string> getPlayersStatus(){
		vector<string> names;
		for (int i = 0; i < (int)players.size(); ++i){
			names.push_back(players[i].getPlayerStatus());
		}
		return names;
	}

	void newPlayer(){
		string line;
		int pid;
		read_log >> pid;
		getline(read_log, line);
		int cut1, cut2;

		cut1 = line.find("n\\")+2;
		cut2 = line.find("t\\")-4;
		string pname = line.substr(cut1, cut2);
		
		if (players.empty()){
			players.push_back(Player(pname, pid));
		}
		else{
			bool in = false;
			for (int i = 0; i < (int)players.size() && !in; ++i){
				if (players[i].player_name == pname){
					in = true;
				}
			}
			if (!in){
				players.push_back(Player(pname, pid));	
			}
		}
	}

	void killCount(){
		const int world = 1022;
		bool suicide = false;
		
		int plr1, plr2, item;

		read_log >> plr1 >>plr2>>item;
		
		total_kills++;
		if (plr1 == world) suicide = true;
		death_by[item]++;
		for (int i = 0; i < (int)players.size(); ++i){//busca o jogador
			if (!suicide && players[i].id == plr1){
				players[i].kill(item);
			}
			else if (players[i].id == plr2){
				if (suicide){
					players[i].suicide(item);
				}
				else{
					players[i].death(item);
				}
			}
		}
	}

	string* deathCauses(){
		char buff[100];
		string *causes = new string[DEATH_TYPES];
		for (int i = 0; i < DEATH_TYPES; ++i){
			sprintf(buff, "deaths by %10s - %3d\n", death_causes[i], death_by[i]);
			causes[i] = string(buff);
		}
		return causes;
	}

	char game_msg[100];
	int total_kills;
	int number_of_games;
	int death_by[DEATH_TYPES];
	
	vector<Player> players;

};	
//*** fim da classe game


int main(int argc, char const *argv[])
{
	if (argc < 2){
		printf("arquivo .log nao informado!\n");
		exit(-1);
	}

	read_log.open(argv[1], ifstream::in);
	write.open("registro.txt", ofstream::out);

	if (!read_log.is_open()){
		printf("arquivo nao encontrado\n");
		exit(-1);
	}
	Game* game = 0;
	int game_count = 1;
	
	while(!read_log.eof()){
		string str; 
		read_log >> str;
		//cout << str << endl;
		if (str == "InitGame:"){
			if (game!=NULL){
				cout << "teste\n";
				delete game;
			}
			game = new Game(game_count++);
		}
		else if (str == "ShutdownGame:"){

			//game->show_players();
			delete game;
			game = NULL;
		}
		else if (str == "Kill:"){
			game->killCount();
		}
		else if (str == "ClientUserinfoChanged:"){
			game->newPlayer();
		}
	}

	return 0;
}