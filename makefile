all: parser
parser: main.o 
	g++ -o parser main.o
#-----> Distancia com o botao TAB ### e nao com espaços
main.o: main.cpp
	g++ -c main.cpp -W -Wall -ansi -pedantic
clean:
	rm -rf *.o
mrproper: clean
	rm -rf teste
